#!/usr/bin/env python2

import argparse
import json
import logging
import requests
import sys
import signal

from lib.ospfv3 import Ospfv3, MSG_TYPES
from lib.ipaddress_serializer import IPAddressEncoder
from lib.NetJSONConverter import NetJSONConverter


def _cleanup_callback():
    _send_message(args.controller,
                  args.port,
                  endpoint="netjsonimport",
                  data=sniffer.get_json(dict=True))


def _send_message(host, port, endpoint, data):
    url = "http://{host}:{port}/{endpoint}".format(host=host, port=port, endpoint=endpoint)
    data = json.dumps(data, cls=IPAddressEncoder)
    headers = {"Content-Type": "application/json"}
    try:
        r = requests.post(url, headers=headers, data=data)
        r.raise_for_status()
        logger.debug("Sent message to {host}".format(host=url))
    except requests.ConnectionError:
        logger.warning("Unable to connect to {url} (Server not running?)".format(url=url))
    except requests.HTTPError as e:
        logger.warning("Server replied with non-success: {message}".format(message=str(e)))


def _sigh(signo,frame):
    logger.info("interrupted, stopping sniffer")
    sniffer.stop()
    logger.info("closing OSPF listener")
    ospf.close()
    sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("RESTful API OSPFv3 topology sniffer")
    parser.add_argument("--controller", "-c", action='store', type=str, required=True,
                        help="Hostname or IP address of the node running the REST server")
    parser.add_argument("--port", "-p", action='store', type=int, required=True,
                        help="Port on which the remote REST server is listening")
    parser.add_argument("--address", "-a", action='store', type=str, default="::",
                        help="IPv6 address of interface to sniff. Default: \"::\" (all interfaces)")
    parser.add_argument("--verbose", "-v", action='count',
                        help="Debug verbosity")
    parser.add_argument("--log-file", action='store', type=str, required=False,
                        help="Optional file to write all log output")

    args = parser.parse_args()

    ospf = Ospfv3(args.address)

    logger = logging.getLogger("sniffer")
    logging.basicConfig(format='%(levelname)s: %(name)s: %(asctime)s %(message)s',
                        filename=args.log_file)
    if (args.verbose == 0): logger.setLevel(logging.WARNING)
    elif (args.verbose == 1): logger.setLevel(logging.INFO)
    elif (args.verbose >= 2): logger.setLevel(logging.DEBUG)

    signal.signal(signal.SIGINT,_sigh)

    sniffer = NetJSONConverter(cleanup_callback=_cleanup_callback, logger=logger)

    try:
        timeout = Ospfv3._holdtimer

        rv = None
        while 1:
            rv = ospf.parseMsg(0, 0)
            type = MSG_TYPES[int(rv['T'])]
            if type == "HELLO":
                logger.debug(
                    "HELLO: %s\n  %s",str(rv['header']),str(rv['body']))
                sniffer.handle_hello(header=rv['header'], body=rv['body'])
            if type == "LSUPD":
                logger.debug(
                    "LSUPD: %s\n  %s",str(rv['header']),str(rv['body']))
                sniffer.handle_lsupd(header=rv['header'], body=rv['body'])

    except KeyboardInterrupt:
        sniffer.stop()
        ospf.close()
        sys.exit(0)
    except Exception as e:
        sniffer.stop()
        logger.exception("OSPF Sniffer crashed due to an uncaught exception")
        sys.exit(-1)
