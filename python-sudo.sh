#!/bin/bash
# Author: Simon Redman <sredman@cs.utah.edu>
# File Created: 18.06.2018
# Last Modified: Mon 25 Apr 2019
# Description: For ease of debugging, it is desirable to have a "fake"
# Python interpreter which runs with elevated permissions. This does that.
#

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

source "${SCRIPTPATH}"/env/bin/activate
sudo $(which python) "$@"
