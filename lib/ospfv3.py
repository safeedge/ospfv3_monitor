#! /usr/bin/env python2.7

##     PyRT: Python Routeing Toolkit

##     OSPFv3 module: provides the OSPF listener and OSPF PDU parsers

##     Copyright (C) 2010 Richard Mortier <mort@cantab.net>
##     Copyright (C) 2017 Binh Nguyen <binh@cs.utah.edu> University of Utah
##     Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>

##     This program is free software; you can redistribute it and/or
##     modify it under the terms of the GNU General Public License as
##     published by the Free Software Foundation; either version 2 of the
##     License, or (at your option) any later version.

##     This program is distributed in the hope that it will be useful,
##     but WITHOUT ANY WARRANTY; without even the implied warranty of
##     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##     General Public License for more details.

##     You should have received a copy of the GNU General Public License
##     along with this program; if not, write to the Free Software
##     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
##     02111-1307 USA

# RFC 1584 -- MOSPF
# RFC ???? -- OSPF v3
# RFC 2328 -- OSPF v2
# RFC 2370 -- Opaque LSAs (updated by RFC 3670)
#   [ This is such a mess compared with IS-IS!  Opaque LSAs have a
#   different LSA header format due to the need to encode an Opaque
#   LSA type ]
# RFC 2676 -- QoS routing mechanisms
# RFC 3101 -- Not-so-stubby-area (NSSA) option
# RFC 3137 -- Stub routers (where metric == 0xffffff > LSInfinity, 0xffff)
# RFC 3623 -- Graceful restart
# RFC 3630 -- Traffic engineering extensions

## LSUPD/LSA notes:

# router id:
#    the IP address of the router that generated the packet
# advrtr:
#    the IP address of the advertising router
# src:
#    the IP address of the interface from which the LSUPD came

# link state id (lsid):
#    identifier for this link (interface) dependent on type of LSA:
#      1 (router)       ID of router generating LSA
#      2 (network)      IP address of DR for LAN
#      3 (summary IP)   IP address of link reported as dst
#      4 (summary ASBR) IP address of reachable ASBR
#      5 (external AS)  IP address of link reported as dst

# link id:
#    what is connected to this router by this link, dependent on type
#      1 (p2p)          ID of neighbour router
#      2 (transit)      IP address of DR for LAN
#      3 (stub)         IP address of LAN (no DR since a stub network)
#      4 (virtual)      ID of neighbour router
# link data:
#    subnet mask if lsid==3; else IP address of the router that
#    generated the LSA on the advertised link (~= advrtr?)

# summary LSA:
#    created by ASBR and flooded into area; type 3 report cost to
#    prefix outside area, type 4 report cost to ASBR

import struct, socket, sys, math, getopt, string, os.path, time, select, traceback
from mutils import *
from collections import namedtuple
import ipaddress

import logging
LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)
PARSE = [1,1,1,1,1] #[hello, dbdesc, req, update, ack]

#-------------------------------------------------------------------------------

INDENT          = "    "
VERSION         = "2.9"

RECV_BUF_SZ      = 8192
OSPF_LISTEN_PORT = 89
LS_INFINITY      = 0xffff
LS_STUB_RTR      = 0xffffff

IPV6_HDR     = "> BBH HBB LLLL LLLL"
IPV6_HDR_LEN = struct.calcsize(IPV6_HDR)

#V3 message structure: https://sites.google.com/site/amitsciscozone/home/important-tips/ipv6/ospfv3-messages
OSPFV3_HDR_DESC = "> BBH L L HBB"
OSPFV3_HDR_LEN  = struct.calcsize(OSPFV3_HDR_DESC)  # type: int
OSPFV3_HDR = namedtuple("OSPFv3_Header", ["version", "type", "length", "router_ID", "area_ID", "checksum", "instance_ID", "zero"])

# OSPFv3 conveniently encodes address prefixes as a length, options, and the prefix bits zero-padded to a multiple of 32
# In this format template, the first byte is the length, the second byte is the options, and the half-word is protocol-specific
# It should be completed by adding enough long words ('L') to capture the correct number of prefix bits
OSPFV3_NET_PREFIX_TEMPLATE = ">BBH {prefix}"
OSPFV3_NET_PREFIX = namedtuple("OSPFV3_Net_Prefix", ["length", "options", "proto_specific",
                                                    "word_1", "word_2", "word_3", "word_4"])
# Since the prefix might be any length, assign all the prefix bits a default value of 0
OSPFV3_NET_PREFIX.__new__.__defaults__ = (0, 0, 0, 0)

OSPFV3_HELLO_FMT = "> L B3s HH L L"
OSPFV3_HELLO_LEN = struct.calcsize(OSPFV3_HELLO_FMT)
OSPFV3_HELLO = namedtuple("OSPFv3_Hello", ["interface_ID", "priority", "options", "hello_interval", "dead_interval", "designated_router", "backup_router", "neighbors"])
OSPFV3_HELLO.__new__.__defaults__ = (None, None)

OSPFV3_DESC     = "> B3s HBB L "
OSPFV3_DESC_LEN = struct.calcsize(OSPFV3_DESC)

OSPFV3_LSREQ     = "> HH L L"
OSPFV3_LSREQ_LEN = struct.calcsize(OSPFV3_LSREQ)

OSPFV3_LSUPD     = "> L"
OSPFV3_LSUPD_LEN = struct.calcsize(OSPFV3_LSUPD)

OSPFV3_LSAHDR_DESC = "> HH L L L HH"
OSPFV3_LSAHDR_LEN  = struct.calcsize(OSPFV3_LSAHDR_DESC)
OSPFV3_LSAHDR_HDR = namedtuple("OSPFv3_LSA_Header", ["age", "type", "ID", "advertising_router", "seq_number", "checksum", "length"])

OSPFV3_LSARTR_DESC = "> B3s"
OSPFV3_LSARTR_LEN  = struct.calcsize(OSPFV3_LSARTR_DESC)
OSPFV3_ROUTER_LSA  = namedtuple("OSPFv3_Router_LSA", ["virtual", "external", "border", "options", "interfaces"])
OSPFV3_LSARTR_INTERFACE_DESC = "> BBH L L L"  # neighbor interface information in router LSA
OSPFV3_LSARTR_INTERFACE_LEN  = struct.calcsize(OSPFV3_LSARTR_INTERFACE_DESC)
OSPFV3_INTERFACE = namedtuple("OSPFv3_Interface", ["ID", "metric", "type", "remote_router_id", "remote_interface_id"])

OSPFV3_LSANET     = "> B3s"
OSPFV3_LSANET_LEN = struct.calcsize(OSPFV3_LSANET)

OSPFV3_LSALINK     = "> B3s LLLL L"
OSPFV3_LSALINK_LEN = struct.calcsize(OSPFV3_LSALINK)

OSPFV3_LSAINTRAPREFIX_HEADER_DESC = "> HH L L "
OSPFV3_LSAINTRAPREFIX_HEADER_LEN = struct.calcsize(OSPFV3_LSAINTRAPREFIX_HEADER_DESC)
OSPFV3_LSAINTRAPREFIX = namedtuple("OSPFv3_Intra_Area_Prefix_LSA", ["number", "type", "ref_ls_ID", "ref_router", "prefixes"])
OSPFV3_LSAINTRAPREFIX.__new__.__defaults__ = (None, None)

#TODO
OSPF_METRIC     = "> BBH"
OSPF_METRIC_LEN = struct.calcsize(OSPF_METRIC)

OSPF_LSASUMMARY     = "> L"
OSPF_LSASUMMARY_LEN = struct.calcsize(OSPF_LSASUMMARY)

OSPF_LSAEXT     = "> L"
OSPF_LSAEXT_LEN = struct.calcsize(OSPF_LSAEXT)

OSPF_LSAEXT_METRIC     = "> BBH L L"
OSPF_LSAEXT_METRIC_LEN = struct.calcsize(OSPF_LSAEXT_METRIC)

################################################################################

DLIST = []

ADDRS = { str2id("224.0.0.5"): "AllSPFRouters",
          str2id("224.0.0.6"): "AllDRouters",
          }
DLIST += [ADDRS]

AFI_TYPES = { 1L: "IP",
              2L: "IP6",
              }
DLIST += [AFI_TYPES]

MSG_TYPES = { 1L: "HELLO",
              2L: "DBDESC",
              3L: "LSREQ",
              4L: "LSUPD",
              5L: "LSACK",
              }
DLIST += [MSG_TYPES]

AU_TYPES = { 0L: "NULL",
             1L: "PASSWD",
             2L: "CRYPTO",
             }
DLIST += [AU_TYPES]

LSAV3_TYPES = {8193: "ROUTER",            # links between routers in the area, 0X2001
               8194: "NETWORK",           # links between "networks" in the area, 0X2002
               8195: "INTER AREA PREFIX",
               8196: "INTER AREA ROUTER",
               16389: "EXTERNAL AS",      # 0X4005

               8198: "GROUP MEMBER",      # 0X2006
               8199: "NSSA",
               8: "LINK LSA",  # 0X0008
               8201: "INTRA AREA PREFIX",  # 0X2009
               }

OPAQUE_TYPES = { 1L: "TRAFFIC ENGINEERING",
                 3L: "GRACEFUL RESTART",
                 }
DLIST += [OPAQUE_TYPES]

TE_TLV_TS = { 1L: "ROUTER ADDRESS",
              2L: "LINK",
              }
DLIST += [TE_TLV_TS]

TE_TLV_LS = { 1L: 4,
              2L: 0, ## variable
              }

TE_LINK_SUBTYPES = { 1L: "TYPE",
                     2L: "ID",
                     3L: "LOCAL IF",
                     4L: "REMOTE IF",
                     5L: "TE METRIC",
                     6L: "MAX BW",
                     7L: "MAX RSVBL BW",
                     8L: "UNRSVD BW",
                     9L: "ADMIN GROUP",
                     }
DLIST += [TE_LINK_SUBTYPES]

TE_LINK_SUBTYPE_LS = { 1L: 1,
                       2L: 4,
                       3L: 4,
                       4L: 4,
                       5L: 4,
                       6L: 4,
                       7L: 4,
                       8L: 32,
                       9L: 4,
                       }

GRACE_TLV_TS = { 1L: "PERIOD",
                 2L: "REASON",
                 3L: "IP ADDR",
                 }
DLIST += [GRACE_TLV_TS]

GRACE_REASONS = { 0L: "UNKNOWN",
                  1L: "SW RESTART",
                  2L: "SW RELOAD/UPGRADE",
                  3L: "SWITCH REDUNDANT RCP",
                  }
DLIST += [GRACE_REASONS]

GRACE_TLV_LS = { 1L: 4,
                 2L: 1,
                 3L: 4,
                 }

RTR_LINK_TYPE = { 1L: "P2P",
                  2L: "TRANSIT",
                  3L: "STUB",
                  4L: "VIRTUAL",
                  }

DLIST += [RTR_LINK_TYPE]

for d in DLIST:
    for k in d.keys():
        d[ d[k] ] = k

################################################################################

def parseOspfHdr(msg, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, msg[:OSPFV3_HDR_LEN])
    header = OSPFV3_HDR(*struct.unpack(OSPFV3_HDR_DESC, msg))
    if verbose > 0:
        print level*INDENT +\
              "OSPF: ver:%s, type:%s, len:%s, rtr id:%s, area id:%s, cksum:%x, instanceid:%s" %\
              (header.version, MSG_TYPES[header.type], len, id2str(header.router_ID), id2str(header.area_ID), header.checksum, header.instance_ID,)

    return header

def parseOspfOpts(opts, verbose=1, level=0):
    if verbose > 1: print "###parseOspfOpts not implemented.###"
    return None
    #TODO
    if verbose > 1: print level*INDENT + int2bin(opts)

    qbit  = (opts & 0x01) ## RFC 2676; reclaim original "T"-bit for TOS routing cap.
    ebit  = (opts & 0x02) >> 1
    mcbit = (opts & 0x04) >> 2
    npbit = (opts & 0x08) >> 3
    eabit = (opts & 0x10) >> 4
    dcbit = (opts & 0x20) >> 5
    obit  = (opts & 0x40) >> 6

    if verbose > 0:
        print level*INDENT + "options: %s %s %s %s %s %s %s" %(
            qbit*"Q", ebit*"E", mcbit*"MC", npbit*"NP", eabit*"EA", dcbit*"DC", obit*"O")

    return { "Q"  : qbit,
             "E"  : ebit,
             "MC" : mcbit,
             "NP" : npbit,
             "EA" : eabit,
             "DC" : dcbit,
             "O"  : obit,
             }

def parseOspfLsaHdr(hdr, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, hdr)
    header = OSPFV3_LSAHDR_HDR(*struct.unpack(OSPFV3_LSAHDR_DESC, hdr))

    if verbose > 0:
        print level*INDENT +\
              "age:%s, type:%s, lsid:%s, advrtr:%s, lsseqno:%s, cksum:%x, len:%s" %(
                  header.age, LSAV3_TYPES[header.type], id2str(header.ID), id2str(header.advertising_router),
                  header.seq_number, header.checksum, header.length)

    return header

def parseOspfLsaRtr(lsa, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, lsa[:OSPFV3_LSARTR_LEN])
    (veb, options) = struct.unpack(OSPFV3_LSARTR_DESC, lsa[:OSPFV3_LSARTR_LEN])
    v = (veb & 0x01)
    e = (veb & 0x02) >> 1
    b = (veb & 0x04) >> 2
    if verbose > 0:
        print level*INDENT + "rtr desc: %s %s %s" %(
            v*"VIRTUAL", e*"EXTERNAL", b*"BORDER")

    lsa = lsa[OSPFV3_LSARTR_LEN:] ; interfaces = []
    while len(lsa) >= OSPFV3_LSARTR_INTERFACE_LEN:
        if verbose > 1: print prtbin((level+1)*INDENT, lsa[:OSPFV3_LSARTR_INTERFACE_LEN])
        (type, _, metric, interfaceid, nbinterfaceid, nbrouterid) = struct.unpack(OSPFV3_LSARTR_INTERFACE_DESC, lsa[:OSPFV3_LSARTR_INTERFACE_LEN])
        if verbose > 0:
            print (level+1)*INDENT +\
                  "type:%s, metric:%s, interfaceid:%s, nbinterfaceid:%s, nbrouterid:%s" %(
                      type, metric, interfaceid, nbinterfaceid, nbrouterid)

        lsa = lsa[OSPFV3_LSARTR_INTERFACE_LEN:] 

        interfaces.append(OSPFV3_INTERFACE(type=type,
                                           metric=metric,
                                           ID=interfaceid,
                                           remote_router_id=nbrouterid,
                                           remote_interface_id=nbinterfaceid))

    return OSPFV3_ROUTER_LSA(virtual=v, external=e, border=b, options=options, interfaces=interfaces)


def parseOspfLsaNet(lsa, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, lsa[:OSPFV3_LSANET_LEN])
    (_, options) = struct.unpack(OSPFV3_LSANET, lsa[:OSPFV3_LSANET_LEN])

    lsa = lsa[OSPFV3_LSANET_LEN:] ; rt_len = struct.calcsize(">L") ; rtrs = []
    while len(lsa) > 0:

        if verbose > 1: print prtbin((level+1)*INDENT, lsa[:OSPFV3_LSANET_LEN])
        (rtr,) = struct.unpack('>L', lsa[:rt_len])
        if verbose > 0:
            print (level+1)*INDENT + "attached rtr:%s" % (id2str(rtr))

        rtrs.append(rtr)
        lsa = lsa[rt_len:]

    return { "options" : options,
             "RTRS" : rtrs
             }

def parseOspfLsaLink(lsa, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, lsa[:OSPFV3_LSALINK_LEN])
    (prio, options, lcp1, lcp2, lcp3, lcp4, nprefix) = struct.unpack(OSPFV3_LSALINK, lsa[:OSPFV3_LSALINK_LEN])
    llprefix = int2ipv6(lcp1, lcp2, lcp3, lcp4)
    if verbose > 0: print (level+1)*INDENT + "link local prefix: %s" % (llprefix)

    lsa = lsa[OSPFV3_LSALINK_LEN:]
    cnt = 0
    prefixes = []
    while cnt < int(nprefix):
        (prefix_len, ) = struct.unpack(">B", lsa[:1])
        prefix_format = OSPFV3_NET_PREFIX_TEMPLATE.format(prefix="L"*(prefix_len//32))
        struct_len = struct.calcsize(prefix_format)
        prefix = OSPFV3_NET_PREFIX(*struct.unpack(
            prefix_format,
            lsa[:struct_len]))
        prefix_str = int2ipv6(prefix.word_1, prefix.word_2, prefix.word_3, prefix.word_4) + "/" + str(prefix.length)
        prefix = ipaddress.ip_network(unicode(prefix_str))

        prefixes.append(prefix)
        lsa = lsa[struct_len:]
        cnt += 1

    return {"options": options,
            "linklocaladdress": llprefix,
            "prefixes": prefixes
            }

def parseOspfLsaIntraAreaPrefix(lsa, verbose=1, level=0):
    if verbose > 1: print prtbin(level * INDENT, lsa[:OSPFV3_LSAINTRAPREFIX_HEADER_LEN])
    prefixes = []
    IntraAreaPrefix = OSPFV3_LSAINTRAPREFIX(*struct.unpack(OSPFV3_LSAINTRAPREFIX_HEADER_DESC, lsa[:OSPFV3_LSAINTRAPREFIX_HEADER_LEN]), prefixes=prefixes)

    # Remove the header from the raw data
    lsa = lsa[OSPFV3_LSAINTRAPREFIX_HEADER_LEN:]
    cnt = 0
    while cnt < int(IntraAreaPrefix.number):
        (prefix_len, ) = struct.unpack(">B", lsa[:1])
        prefix_format = OSPFV3_NET_PREFIX_TEMPLATE.format(prefix="L"*(prefix_len//32))
        struct_len = struct.calcsize(prefix_format)
        prefix = OSPFV3_NET_PREFIX(*struct.unpack(
            prefix_format,
            lsa[:struct_len]))
        prefix_str = int2ipv6(prefix.word_1, prefix.word_2, prefix.word_3, prefix.word_4) + "/" + str(prefix.length)
        prefix = ipaddress.ip_network(unicode(prefix_str))
        prefixes.append(prefix)
        lsa = lsa[struct_len:]
        cnt += 1

    return IntraAreaPrefix

def parseOspfLsaSummary(lsa, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, lsa[:OSPF_LSASUMMARY_LEN])
    (mask, ) = struct.unpack(OSPF_LSASUMMARY, lsa[:OSPF_LSASUMMARY_LEN])
    if verbose > 0:
        print level*INDENT + "mask:%s" % (id2str(mask), )

    lsa = lsa[OSPF_LSASUMMARY_LEN:] ; cnt = 0 ; metrics = {}
    while len(lsa) > 0:
        cnt += 1

        if verbose > 1: print prtbin((level+1)*INDENT, lsa[:OSPF_METRIC_LEN])
        (tos, stub, metric) = struct.unpack(OSPF_METRIC, lsa[:OSPF_METRIC_LEN])

        ## RFC 3137 "Stub routers": if (stub,metric) == (0xff, 0xffff)
        ## then this is a stub router and it is attempting to
        ## discourage other routers from using it to transit traffic,
        ## ie. forward traffic to any networks others than those
        ## connected directly

        metric = ((stub<<16) | metric)
        if verbose > 0:
            if metric == LS_STUB_RTR: mstr = "metric:STUB_ROUTER"
            elif metric > LS_INFINITY: mstr = "*** metric:%s > LS_INFINITY! ***" % metric
            elif metric == LS_INFINITY: mstr = "metric:LS_INFINITY"
            else: mstr = "metric:%d" % metric
            print (level+1)*INDENT + "%s: tos:%s, %s" % (cnt, tos, mstr)

        metrics[tos] = metric
        lsa = lsa[OSPF_METRIC_LEN:]

    return { "MASK"    : mask,
             "METRICS" : metrics
             }

def parseOspfLsaExt(lsa, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, lsa[:OSPF_LSAEXT_LEN])
    (mask, ) = struct.unpack(OSPF_LSAEXT, lsa[:OSPF_LSAEXT_LEN])
    if verbose > 0: print level*INDENT + "mask:%s" % id2str(mask)

    lsa = lsa[OSPF_LSAEXT_LEN:] ; cnt = 0 ; metrics = {}
    while len(lsa) > 0:

        if verbose > 1: print prtbin((level+1)*INDENT, lsa[:OSPF_LSAEXT_METRIC_LEN])
        (exttos, stub, metric, fwd, tag, ) =\
           struct.unpack(OSPF_LSAEXT_METRIC, lsa[:OSPF_LSAEXT_METRIC_LEN])
        ext = ((exttos & 0xf0) >> 7) * "E"
        tos = exttos & 0x7f

        metric = ((stub<<16) | metric)
        if verbose > 0:
            if metric == LS_STUB_RTR: mstr = "metric:STUB_ROUTER"
            elif metric > LS_INFINITY: mstr = "*** metric:%s > LS_INFINITY! ***" % metric
            elif metric == LS_INFINITY: mstr = "metric:LS_INFINITY"
            else: mstr = "metric:%d" % metric
            print (level+1)*INDENT +\
                  "%s: ext:%s, tos:%s, %s, fwd:%s, tag:0x%x" %(
                      cnt, ext, int2bin(tos), mstr, id2str(fwd), tag)

        metrics[tos] = { "EXT"    : ext,
                         "METRIC" : metric,
                         "FWD"    : fwd,
                         "TAG"    : tag,
                         }

        lsa = lsa[OSPF_LSAEXT_METRIC_LEN:]
        cnt += 1

    return { "MASK": mask,
             "METRICS": metrics,
             }

def parseOspfLsas(lsas, verbose=1, level=0):
    """
    Process an incoming packet which may contain one or more LSAs

    :param lsas: Incoming packet body containing LSAs
    :param verbose:
    :param level:
    :return: List of LSAs parsed to Python data structures
    """

    rv = []

    cnt = 0
    while len(lsas) > 0:
        rv.append({})

        if verbose > 0: print level*INDENT + "LSA %s" % cnt
        header = parseOspfLsaHdr(lsas[:OSPFV3_LSAHDR_LEN], verbose, level+1)
        rv[cnt]["header"] = header
        t = header.type
        l = header.length
        rv[cnt]["type"] = header.type
        rv[cnt]["length"] = header.length

        LSA_type = LSAV3_TYPES[header.type]

        lsa = lsas[OSPFV3_LSAHDR_LEN:l]

        if LSA_type == "ROUTER":
            rv[cnt]["body"] = parseOspfLsaRtr(lsa, verbose, level+1)
        elif LSA_type == "NETWORK":
            rv[cnt]["body"] = parseOspfLsaNet(lsa, verbose, level+1)
        elif LSA_type == "LINK LSA":
            rv[cnt]["body"] = parseOspfLsaLink(lsa, verbose, level+1)
        elif LSA_type == "INTRA AREA PREFIX":
            rv[cnt]["body"] = parseOspfLsaIntraAreaPrefix(lsa, verbose, level+1)

        else:
            error("[ *** unknown LSA type %d*** ]\n" % (t, ))
            error("%s\n" % prtbin(level*INDENT, msg))

        lsas = lsas[l:]
        cnt += 1

    return rv

def parseOspfHello(msg, verbose=1, level=0):
    """
    Parse the body of an OSPFv3 HELLO message

    :param msg: HELLO message to parse
    :param verbose: Print level
    :param level: Print indentation
    :return: Tuple(body, neighbors)
    """

    if verbose > 1:
        print prtbin(level*INDENT, msg)

    hello = OSPFV3_HELLO(*struct.unpack(OSPFV3_HELLO_FMT, msg[:OSPFV3_HELLO_LEN]))

    if verbose > 0:
        print level*INDENT +\
              "HELLO: interfaceid:%s, prio:%s, opts:%s, hello interval:%s, dead intvl:%s" %\
              (hello.interface_ID, hello.priority, hello.options, hello.hello_interval, hello.dead_interval)
        print (level+1)*INDENT +\
              "designated rtr:%s, backup designated rtr:%s" %\
              (id2str(hello.designated_router), id2str(hello.backup_router))

    raw_neighbors = msg[OSPFV3_HELLO_LEN:]
    nbor_len = struct.calcsize(">L")
    neighbors = []
    while len(raw_neighbors) > 0:
        if verbose > 1: print prtbin(level*INDENT, raw_neighbors[:nbor_len])
        (neighbor,) = struct.unpack(">L", raw_neighbors[:nbor_len])
        if verbose > 0:
            print (level+1)*INDENT + "neighbour: %s" % (id2str(neighbor),)
        neighbors.append(neighbor)
        raw_neighbors = raw_neighbors[nbor_len:]

    # namedtuple is not mutable:
    # re-construct a new OSPFV3_HELLO with the neighbors list
    hello = OSPFV3_HELLO(*list(hello)[:-1], neighbors=neighbors)
    return hello

def parseOspfDesc(msg, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, msg)
    (zero, opts, mtu, aopts, imms, ddseqno) = struct.unpack(OSPFV3_DESC, msg[:OSPFV3_DESC_LEN])
    init        = (imms & 0x04) >> 2
    more        = (imms & 0x02) >> 1
    masterslave = (imms & 0x01)
    if verbose > 0:
        print level*INDENT +\
              "DESC: mtu:%s, opts:%s, imms:%s%s%s%s, dd seqno:%s" %\
              (mtu, opts, init*"INIT", more*" MORE",
               masterslave*" MASTER" ,(1-masterslave)*" SLAVE",
               ddseqno)

    return { "MTU"         : mtu,
             "OPTS"        : parseOspfOpts(opts, verbose, level),
             "INIT"        : init,
             "MORE"        : more,
             "MASTERSLAVE" : masterslave,
             }

def parseOspfLSReq(msg, verbose=1, level=0):

    error("### LSREQ UNIMPLEMENTED ###\n")
    return None

def parseOspfLsUpd(msg, verbose=1, level=0):

    if verbose > 1: print prtbin(level*INDENT, msg[:OSPFV3_LSUPD_LEN])
    (nlsas, ) = struct.unpack(OSPFV3_LSUPD, msg[:OSPFV3_LSUPD_LEN])
    if verbose > 0:
        print level*INDENT + "LSUPD: nlsas:%s" % (nlsas)

    return { "NLSAS" : nlsas,
             "LSAS"  : parseOspfLsas(msg[OSPFV3_LSUPD_LEN:], verbose, level+1),
             }

def parseOspfLsAck(msg, verbose=1, level=0):

    if verbose > 0: print level*INDENT + "LSACK"

    cnt = 0 ; lsas = {}
    while len(msg) > 0:
        cnt += 1
        if verbose > 0: print (level+1)*INDENT + "LSA %s" % cnt
        lsas[cnt] = parseOspfLsaHdr(msg[:OSPFV3_LSAHDR_LEN], verbose, level+1)
        msg = msg[OSPFV3_LSAHDR_LEN:]

    return { "LSAS"  : lsas
             }

def parseOspfMsg(msg, verbose=1, level=0):

    ospfh = parseOspfHdr(msg[:OSPFV3_HDR_LEN], verbose, level)
    rv = { "T": ospfh.type,
           "L": len(msg),
           "header": ospfh,
           "body": None,
           }

    message_type = MSG_TYPES[ospfh.type]

    if message_type == "HELLO" and (PARSE[0] == 1):
        rv["body"] = parseOspfHello(msg[OSPFV3_HDR_LEN:], verbose, level+1)

    elif message_type == "DBDESC" and (PARSE[1] == 1):
        rv["body"] = parseOspfDesc(msg[OSPFV3_HDR_LEN:], verbose, level+1)

    elif message_type == "LSREQ" and (PARSE[2] == 1):
        rv["body"] = parseOspfLSReq(msg[OSPFV3_HDR_LEN:], verbose, level)

    elif message_type == "LSUPD" and (PARSE[3] == 1):
        rv["body"] = parseOspfLsUpd(msg[OSPFV3_HDR_LEN:], verbose, level+1)

    elif message_type == "LSACK" and (PARSE[4] == 1):
        rv["body"] = parseOspfLsAck(msg[OSPFV3_HDR_LEN:], verbose, level+1)

    return rv

################################################################################

class OspfExc(Exception): pass

class Ospfv3:

    _version   = 2
    _holdtimer = 30

    #---------------------------------------------------------------------------

    class Adj:

        def __init__(self): pass
        def __repr__(self): pass

    #---------------------------------------------------------------------------

    def __init__(self, ADDRESS):

        ## XXX raw sockets are broken in Windows Python (some madness
        ## about linking against winsock1, etc); applied "patch" from
        ## https://sourceforge.net/tracker/?func=detail&atid=355470&aid=889544&group_id=5470,
        ## http://www.rs.fromadia.com/newsread.php?newsid=254,
        ## http://www.rs.fromadia.com/files/pyraw.exe to fix this

        #self._sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_IP)
        #self._sock = socket.socket(socket.AF_INET6, socket.SOCK_RAW, socket.IPPROTO_IPV6)
        #self._sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
	self._sock = socket.socket(socket.AF_INET6, socket.SOCK_RAW, 89)
	#res = socket.getaddrinfo("localhost", 2000, socket.AF_INET6, socket.SOCK_RAW, socket.IPPROTO_IPV6)
	#af, socktype, proto, canonname, sa = res[1]

        #self._sock = socket.socket(af, socktype, proto)
        self._addr = (ADDRESS, 0)
        self._sock.bind(self._addr)
        self._name = self._sock.getsockname()

        #self._sock.setsockopt(89, socket.IP_HDRINCL, 1)
	#self._sock.listen(1)
        #self._sock.ioctl(socket.SIO_RCVALL, 1)

        self._adjs = {}
        self._rcvd = ""
	

    def __repr__(self):

        rs = """OSPF listener, version %s:
        socket:  %s
        address: %s, name: %s""" %\
            (self._version, self._sock, self._addr, self._name)

        return rs

    def close(self):

        self._sock.close()

    #---------------------------------------------------------------------------

    def parseMsg(self, verbose=1, level=0):
        rv = None
        try:
            (msg_len, msg) = self.recvMsg(verbose, level)

        except OspfExc, oe:
            if verbose > 1: print "[ *** Non OSPF packet received *** ]"
            return

        if verbose > 2:
            print "%sparseMsg: len=%d%s" %\
                  (level*INDENT, msg_len, prthex((level+1)*INDENT, msg))

        rv = parseOspfMsg(msg, verbose, level)

        return rv

    def recvMsg(self, verbose=1, level=0):
        self._rcvd = self._sock.recv(RECV_BUF_SZ)
        if verbose > 2:
            print "%srecvMsg: recv: len=%d%s" %\
                  (level*INDENT,
                   len(self._rcvd), prthex((level+1)*INDENT, self._rcvd))

        return (len(self._rcvd), self._rcvd)

    def sendMsg(self, verbose=1, level=0):
        pass