# Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from threading import Condition, Lock, Thread, Event
from netdiff import NetJsonParser
import networkx
from Queue import Queue, Empty
import sys
import time

from ospfv3 import LSAV3_TYPES, OSPFV3_ROUTER_LSA


class MyThread(Thread):
    def __init__(self,*args,**kwargs):
        super(MyThread,self).__init__(*args,**kwargs)
        self._stop = Event()

    def stop(self):
        self._stop.set()

    def is_stopped(self):
        return self._stop.is_set()

    def run(self):
        self._stop.clear()
        return super(MyThread,self).run()


class NetJSONConverter:
    """
    Convert a stream of OSPF messages into NetJSON
    """

    def __init__(self, same_ID_cutoff=3, cleanup_callback=None, logger=None):
        """

        :param same_ID_cutoff: Number of times we should identifiy the same ID as being ours before accepting it
        :param cleanup_callback: Optionally call an external function after the graph is pruned of dead neighbors
        :param logger: Logger to use, otherwise no output will be printed
        """
        self.NetJsonParser = NetJsonParser(data={"type": "NetworkGraph",
                                                 "protocol":"OSPF",
                                                 "version":"3",
                                                 "metric":"SPF",
                                                 "nodes": [],
                                                 "links": []})
        self.NetJsonParser.graph = networkx.DiGraph()
        self.graph = self.NetJsonParser.graph
        self.graph_lock = Lock()

        self.hello_neighbors_intersection = set()  # Nodes which we have seen as neighbors IN ALL Hello packets
        self.this_node_ID = None  # The OSPF ID belonging to this node
        self.same_node_ID_count = 0  # To hopefully make sure we have correctly identified the node_ID,
                                     # count how many times we have identified the same value
        self.same_ID_cutoff = same_ID_cutoff
        self.ID_stable = False

        self.dead_interval = None   # OSPF dead interval to determine when a router is unreachable
        self.hello_interval = None  # OSPF hello interval

        self.known_router_LSAs = {}  # Store a mapping of Router IDs to the Router LSA which introduced them
        self.known_network_LSAs = {}  # Store a mapping of Router IDs to the Network LSAs which introduced them

        self.unprocessed_LSUPDs = Queue()
        self.queue_processor = MyThread(target=self._process_lsupd_queue)

        self.cleanup_callback = cleanup_callback
        self.cleanup_thread = MyThread(target=self._cleanup_graph_thread)
        # Any node put in unreachable_router is removed from the graph by the cleaner thread
        # If cleaning any node makes other nodes unreachable from this node, those nodes are also deleted
        self.unreachable_routers = Queue()

        self.logger = logger
        if self.logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.setLevel(logging.CRITICAL)  # Sorta disable this logger

        self.crash_report = None  # Query this to see if a thread has crashed... How lame!

    def stop(self,timeout=None):
        if self.cleanup_thread.is_alive():
            self.cleanup_thread.stop()
            self.cleanup_thread.join(timeout)
        if self.queue_processor.is_alive():
            self.queue_processor.stop()
            self.queue_processor.join(timeout)

    def handle_hello(self, header, body):
        """
        Accept an OSPF Hello message into the graph

        :param header: OSPFV3 Header corresponding to the incoming packet
        :type header:  OSPFv3_Header
        :param body:   OSPFV3_Hello parsed from the incoming message
        :type header:  OSPFv3_Hello
        :return:
        :rtype: Dict[List[int]]
        """
        self._check_thread_livelieness()
        sender = header.router_ID

        # Update our list of neighbors and the time they last hello'ed
        with self.graph_lock:
            self.graph.add_node(sender)
            self.graph.nodes[sender]['last_hello'] = time.time()

            # Add edges between this node and its (new) neighbor
            if self.ID_stable:
                    fwd_edge = (self.this_node_ID, sender)
                    back_edge = (sender, self.this_node_ID)
                    for edge in [fwd_edge, back_edge]:
                        self.graph.add_edge(*edge)
                        if not 'weight' in self.graph.edges(edge):
                            self.graph.edges[edge]['weight'] = None

        # Use HELLOs to determine the ID of the node the sniffer is running on
        # The idea is basically that all of this node's neighbors will be sending
        # HELLOs containing this node's ID
        # Assuming no nodes have a "twin" which shares exactly the same neighbors
        # (Maybe not a good assumption in practice!), whichever node is in all of
        # the received HELLO messages is this node, since this node is the only
        # one visible to all of its neighbors
        if not self.ID_stable:
            if len(self.hello_neighbors_intersection) == 0:
                self.hello_neighbors_intersection = set(body.neighbors)
            else:
                self.hello_neighbors_intersection.intersection_update(body.neighbors)
                if len(self.hello_neighbors_intersection) == 1:
                    old_ID = self.this_node_ID
                    self.this_node_ID = self.hello_neighbors_intersection.pop()
                    if old_ID == self.this_node_ID:
                        self.same_node_ID_count += 1
                    else:
                        self.same_node_ID_count = 0
                    if self.same_node_ID_count >= self.same_ID_cutoff:
                        self.ID_stable = True
                        self.dead_interval = body.dead_interval
                        self.hello_interval = body.hello_interval

                        self.graph.add_node(self.this_node_ID)

                        self.cleanup_thread.start()
                        self.queue_processor.start()

                        # Cleanup variables which will no longer be used to aid debug-ability
                        del self.same_ID_cutoff
                        del self.same_node_ID_count
                        del self.hello_neighbors_intersection
        pass

    def handle_lsupd(self, header, body):
        """
        Enqueue one or more Link State Update messages for processing

        :param header: OSPFv3 Header
        :param body:   OPSFv3 Link State Update packet
        :return: None
        """
        self._check_thread_livelieness()
        for LSUPD in body['LSAS']:
            ls_header = LSUPD['header']
            ls_body = LSUPD['body']

            self.unprocessed_LSUPDs.put((ls_header, ls_body), block=False)
        return

    def get_json(self, **kwargs):
        """
        Get a json representation of the current graph

        :param kwargs: Arguments to be passed to netdiff's .json() method
        :return: json representation of the current graph
        """
        with self.graph_lock:
            return self.NetJsonParser.json(**kwargs)

    def _check_thread_livelieness(self):
        """
        Check if the processor thread has left a crash report and raise it if it has
        """
        if self.crash_report:
            raise self.crash_report[1], None, self.crash_report[2]

    def _process_lsupd_queue(self):
        try:
            while True:
                (ls_header,ls_body) = (None,None)
                try:
                    ls_header, ls_body = self.unprocessed_LSUPDs.get(block=False, timeout=0.1)
                except Empty:
                    if self.queue_processor.is_stopped():
                        return
                    continue
                with self.graph_lock:
                    while True:
                        self._process_lsa(ls_header, ls_body)
                        try:
                            ls_header, ls_body = self.unprocessed_LSUPDs.get(block=True, timeout=1)
                        except Empty:
                            break
                    # Do any cleanup and send a callback as soon as anything changes
                    self._cleanup_graph()
                self.cleanup_callback()
        except Exception as e:
            self.crash_report = 1  # Set this to something "easy" in case there is a secondary crash
            self.crash_report = sys.exc_info()
            return

    def _process_lsa(self, ls_header, ls_body):
        """
        Process a single OSPFv3 Link State Advertisement

        :param ls_header: Header corresponding to this LSA
        :param ls_body:   Body corresponding to this LSA
        :return: None
        """
        source = ls_header.advertising_router
        type = LSAV3_TYPES[ls_header.type]

        self.logger.info("Processing {type} LSA".format(type=type))

        if type == "ROUTER":
            """
            https://tools.ietf.org/html/rfc2740.html#page-61
            Describe the state and cost of a router's interfaces to the area
            
            Many of this type of LSA may be sent, in which case the list of interfaces should be treated
            as if they were concatenated
            In particular, if the list should *not* be concatenated, the sequence number will update
            
            We receive one of these:
                - While the network is initializing (and every 30 minutes thereafter)
                - When a link goes down
                    - In which case the interface corresponding to the link is not in the list of interfaces
            """
            # Convert the list of interfaces to a dictionary keyed by interface ID
            iface_map = {}
            for interface in ls_body.interfaces:
                iface_map[interface.ID] = interface
            ls_body = OSPFV3_ROUTER_LSA(*list(ls_body)[:-1], interfaces=iface_map)

            if source not in self.known_router_LSAs:
                # If we haven't heard anything from this router yet, start from scratch
                self.known_router_LSAs[source] = (ls_header, ls_body)
            else:
                known_header, known_body = self.known_router_LSAs[source]
                if ls_header.seq_number == known_header.seq_number:
                    # Since the sequence number has not changed, this packet represents a continuation
                    # of the previous list of interfaces
                    # Append to the list of known interfaces
                    known_body.interfaces.update(ls_body.interfaces)
                else:
                    # Since the sequence number has changed we are receiving a new list of interfaces
                    # Start afresh
                    self.known_router_LSAs[source] = (ls_header, ls_body)
            self.graph.add_node(source)  # Sometimes early on we get a node with no interfaces. Make note of it for later

            # In case we extended a known list of interfaces, update the version we are working on
            (ls_header, ls_body) = self.known_router_LSAs[source]

            known_interfaces = set()
            for neighbor in self.graph.adj[source]:
                edge = self.graph.edges[source, neighbor]
                if 'interface_ID' in edge:
                    known_interfaces.add(edge['interface_ID'])

            for interface in ls_body.interfaces.values():
                edge = (source, interface.remote_router_id)
                interface_ID = interface.ID
                self.graph.add_edge(*edge,
                                    weight=int(interface.metric),
                                    interface_ID=interface_ID)
                if interface_ID in known_interfaces:
                    known_interfaces.remove(interface_ID)

            # If there are any interfaces which we knew of before but which were not just advertised,
            # those indicate a downed link
            dead_links = set()
            for interface in known_interfaces:
                # Remove the link to the router on the other side of the down interface
                for neighbor in self.graph.adj[source]:
                    edge = self.graph.edges[source, neighbor]
                    if 'interface_ID' in edge and edge['interface_ID'] == interface:
                        dead_links.add((source, neighbor))
            for link in dead_links:
                self.graph.remove_edge(*link)

            pass

        elif type == "NETWORK":
            """
            https://tools.ietf.org/html/rfc2740.html#page-64
            Describe all routers attached to a link
            """
            self.known_network_LSAs[source] = (ls_header, ls_body)
            routers = ls_body['RTRS']
            for router in routers:
                if router == source: continue
                edge = (source, router)
                self.graph.add_edge(*edge)
                if not 'weight' in self.graph.edges[edge]:
                    # For OSPF graph, all edges must have a weight. Until we know more, stuff some junk in there
                    self.graph.edges[edge]['weight'] = None
                self.graph.edges[edge]['interface_ID'] = ls_header.ID

        elif type == "INTER AREA PREFIX":
            raise NotImplementedError()

        elif type == "INTER AREA ROUTER":
            """
            https://tools.ietf.org/html/rfc2740.html#page-66
            Describe routes to routers in other areas
            """
            raise NotImplementedError()
        elif type == "EXTERNAL AS":
            """
            https://tools.ietf.org/html/rfc2740.html#page-67
            Describe destinations external to the AS

            Not used in my project, therefore not implemented
            """
            raise NotImplementedError()

        elif type == "LINK LSA":
            """
            https://tools.ietf.org/html/rfc2740.html#page-69
            Link-type LSAs are used to provide link-local addresses to other
            routers on the link as well as tell what prefixes are on the link
            """
            edge = (source, self.this_node_ID)
            # Put this edge in the graph if it is not already
            self.graph.add_edge(*edge)
            # Add the newly advertised prefixes to the currently known prefixes
            if not 'prefixes' in self.graph.edges[edge]:
                self.graph.edges[edge]['prefixes'] = []

            # Store prefixes as a list, because JSON does not support sets, but
            # enforce uniqueness by shoveling through a set first
            unique_prefixes = set(self.graph.edges[edge]['prefixes'])
            unique_prefixes.update(ls_body['prefixes'])
            self.graph.edges[edge]['prefixes'] = list(unique_prefixes)

            # OSPF requires that all links have weights. Until we know more, put some junk in there
            if not 'weight' in self.graph.edges[edge]:
                self.graph.edges[edge]['weight'] = None

        elif type == "INTRA AREA PREFIX":
            """
            https://tools.ietf.org/html/rfc2740.html#page-71
            Describe IPv6 prefixes associated with the router itself or an attached network segment
            """
            ref_type = LSAV3_TYPES[ls_body.type]

            if ref_type == "ROUTER":
                try:
                    ref_header, ref_body = self.known_router_LSAs[ls_body.ref_router]
                except KeyError as e:
                    # Sometimes we miss an earlier LSA (or one which was sent from this node)
                    # There is nothing to be done in this case except keep hobbling along
                    self.logger.warning("Missed a router LSA from {ref}".format(ref=ls_body.ref_router))
                    return
                ref_router = ref_header.advertising_router
                self.graph.add_node(ref_router)  # Necessary if a previously-known router reappears
                if not 'prefixes' in self.graph.nodes[ref_router]:
                    self.graph.nodes[ref_router]['prefixes'] = []
                # Need to store a list in the graph, but use set temporarily to ensure uniqueness
                unique_prefixes = set(self.graph.nodes[ref_router]['prefixes'])
                unique_prefixes.update(ls_body.prefixes)
                self.graph.nodes[ref_router]['prefixes'] = list(unique_prefixes)
            elif ref_type == "NETWORK":
                try:
                    ref_header, ref_body = self.known_network_LSAs[ls_body.ref_router]
                except KeyError as e:
                    # Sometimes we miss an earlier LSA (or one which was sent from this node)
                    # There is nothing to be done in this case except keep hobbling along
                    self.logger.warning("Missed a network LSA from {ref}".format(ref=ls_body.ref_router))
                    return
                ref_router = ref_header.advertising_router

                routers = ref_body['RTRS']
                for router in routers:
                    if router == ref_router: continue
                    fwd_edge = (ref_router, router)
                    back_edge = (router, ref_router)
                    for edge in fwd_edge, back_edge:
                        if edge not in self.graph.edges:
                            return  # This LSA is probably informing us of a downed link
                        self.graph.edges[edge]['prefixes'] = ls_body.prefixes
            else:
                raise ValueError("Intra Area Prefix LSA is malformed")
        else:
            raise NotImplementedError("Unexpected type encountered")

    def _cleanup_graph_thread(self):
        try:
            while True:
                wait_until = time.time() + self.hello_interval
                with self.graph_lock:
                    self._cleanup_graph()
                if self.cleanup_callback is not None:
                    self.cleanup_callback()
                while wait_until > time.time():
                    if self.cleanup_thread.is_stopped():
                        return
                    time.sleep(0.1)

        except Exception as e:
            self.crash_report = 1  # Set this to something "easy" in case there is a secondary crash
            self.crash_report = sys.exc_info()
            return

    def _cleanup_graph(self):
        assert self.this_node_ID in self.graph.nodes, "This node not in its own graph!"
        # Cleanup any neighbors who have not hello'ed recently enough
        for neighbor in self.graph.adj[self.this_node_ID]:
            if 'last_hello' not in self.graph.nodes[neighbor]:
                continue  # This probably indicates a problem, but just skip it
            else:
                last_hello = self.graph.nodes[neighbor]['last_hello']
            hello_distance = time.time() - last_hello
            if hello_distance > self.dead_interval:
                # This router is down!
                self.unreachable_routers.put(neighbor)

        # Remove any nodes which have been marked as unreachable
        while not self.unreachable_routers.empty():
            unreachable_node = self.unreachable_routers.get_nowait()
            assert unreachable_node != self.this_node_ID, "Trying to delete this node from its own graph!"
            self.graph.remove_node(unreachable_node)

        # Traverse the graph to find any more unreachable nodes
        visited_nodes = set()
        fringe = Queue()
        fringe.put(self.this_node_ID)

        while not fringe.empty():
            node = fringe.get_nowait()
            visited_nodes.add(node)
            for neighbor in self.graph.adj[node]:
                if neighbor not in visited_nodes:
                    fringe.put(neighbor)

        # Nodes which were not visited are unreachable
        for node in self.graph.nodes:
            if node not in visited_nodes:
                self.unreachable_routers.put(node)

        # Clean up one more time
        while not self.unreachable_routers.empty():
            unreachable_node = self.unreachable_routers.get_nowait()
            assert unreachable_node != self.this_node_ID, "Trying to delete this node from its own graph!"
            self.graph.remove_node(unreachable_node)
