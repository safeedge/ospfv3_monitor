# Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ipaddress
import json

class IPAddressEncoder(json.JSONEncoder):
    """
    JSON encode an IPAddress object
    """

    def default(self, obj):
        if isinstance(obj, ipaddress.IPv4Address) or isinstance(obj, ipaddress.IPv6Address):
            raise NotImplementedError()
        elif isinstance(obj, ipaddress.IPv4Network) or isinstance(obj, ipaddress.IPv6Network):
            return { "prefix": str(obj.network_address), "length": obj.prefixlen }
        elif isinstance(obj, ipaddress.IPv4Interface) or isinstance(obj, ipaddress.IPv6Interface):
            raise NotImplementedError()
        else:
            return json.JSONEncoder.default(self, obj)